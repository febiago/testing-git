const { User_game } = require('../models');
const googleOauth2 = require('../utils/google');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const emails = require('../utils/email');
const { gmail } = require('googleapis/build/src/apis/gmail');
const { use } = require('../routes');

const {
    JWT_SIGNATURE_KEY
} = process.env;

module.exports = {
    google: async (req, res, next) => {
        try {
            const code = req.query.code;

            if (!code) {
                const url = googleOauth2.generateAuthURL();
                return res.redirect(url);
            }
            await googleOauth2.setCredentials(code);
            const { data }  = await googleOauth2.getUserData();

            var userExist = await User_game.findOne({ where: { email: data.email } });

            const encryptedPassword = await bcrypt.hash('pass123', 10);

            if (!userExist) {
                userExist = await User_game.create({
                    name: data.name,
                    email: data.email,
                    password: encryptedPassword,
                    user_type: 'google',
                    role: 'user'
                });

            }
            // generate token
            const payload = {
                id: userExist.id,
                name: userExist.name,
                email: userExist.email,
                role: userExist.role,
            };
            const token = jwt.sign(payload, JWT_SIGNATURE_KEY);

            // return token 
            return res.status(200).json({
                status: true,
                message: 'success',
                data: {
                    id_user: userExist.id,
                    token
                }
            });
        } catch (err) {
            next(err);
        }
    },

    register: async (req, res, next) => {
        try {
            const { name, email, password, user_type, role } = req.body;

            const existUser = await User_game.findOne({ where: { email: email } });
            if (existUser) {
                return res.status(409).json({
                    status: false,
                    message: 'email already used!'
                });
            }

            const encryptedPassword = await bcrypt.hash(password, 10);
            const user = await User_game.create({
                name,
                email,
                password: encryptedPassword,
                user_type,
                role
            });

            const link = 'http://localhost:3000/welcome';
            const htmlWelcome = await emails.getHtml('welcome.ejs', { name: data.name, link: link });
            await emails.sendEmail(data.email, 'Welcome to apps challange 8', htmlWelcome);
                
            return res.status(201).json({
                status: true,
                message: 'register success',
                data: {
                    name: user.name,
                    email: user.email,
                    role: user.role
                }
            });
        } catch (err) {
            next(err);
        }
    },

    login: async (req, res, next) => {
        try {
            const { email, password } = req.body;

            const user = await User_game.findOne({ where: { email: email } });
            if (!user) {
                return res.status(400).json({
                    status: false,
                    message: 'email or password doesn\'t match!'
                });
            }

            const correct = await bcrypt.compare(password, user.password);
            if (!correct) {
                return res.status(400).json({
                    status: false,
                    message: 'user or password doesn\'t match!'
                });
            }
            // generate token
            payload = {
                id: user.id,
                name: user.name,
                email: user.email,
                user_type: user.user_type,
                role: user.role
            };
            const token = jwt.sign(payload, JWT_SIGNATURE_KEY);
            const decoded = jwt.verify(token, JWT_SIGNATURE_KEY);
            req.id = decoded;

            if (decoded.user_type == "google") {
              return res.status(403).json({
                status: false,
                message: "This user is logged in using a google account!",
              });
            }
            return res.status(200).json({
                status: true,
                message: 'success',
                data: {
                    token: token
                }
            });
        } catch (err) {
            next(err);
        }
    },

    profil: (req, res, next) => {
        const user = req.user;

        try {
            return res.status(200).json({
                status: true,
                message: 'success',
                data: user
            });
        } catch (err) {
            next(err);
        }
    },

    changePassword: async (req, res, next) => {
            try {
                const { oldPassword, newPassword, confirmNewPassword } = req.body;
                
                if (newPassword !== confirmNewPassword) {
                    return res.status(422).json({
                        status: false,
                        message: 'new password and confirm new password doesnt match!'
                    });
                }
    
                const user = await User_game.findOne({ where: { id: req.user.id } });
                if (!user) 
                    return res.status(404).json({ 
                        success: false, 
                        message: 'User not found!' 
                    });
                
                const correct = await bcrypt.compare(oldPassword, user.password, );
                if (!correct) {
                    return res.status(400).json({
                        status: false,  
                        message: 'old password does not match!'
                    });
                }
    
                const encryptedPassword = await bcrypt.hash(newPassword, 10);
                const updatedUser = await User_game.update({
                    password: encryptedPassword
                }, {
                    where: {
                        id: user.id
                    }
                });
    
                return res.status(200).json({
                    status: true,
                    message: 'success',
                    data: updatedUser
                });
    
            } catch (err) {
                next(err);
            }
},

    forgotPasswordView: (req, res) => {
        return res.render('auth/forgot-password', { message: null });
    },

    forgotPassword: async (req, res, next) => {
        try {
            const { email } = req.body;

            const user = await User.findOne({ where: { email } });
            if (user) {
                const payload = { user_id: user.id };
                const token = jwt.sign(payload, JWT_SECRET_KEY);
                const link = `http://localhost:3000/auth/reset-password?token=${token}`;

                htmlEmail = await util.email.getHtml('reset-password.ejs', { name: user.name, link: link });
                await util.email.sendEmail(user.email, 'Reset your password', htmlEmail);
            }

            return res.render('auth/forgot-password', { message: 'we will send email for reset password if the email is exist on our database!' });
        } catch (err) {
            next(err);
        }
    },

    resetPasswordView: (req, res) => {
        const { token } = req.query;
        return res.render('auth/reset-password', { message: null, token });
    },

    resetPassword: async (req, res, next) => {
        try {
            const { token } = req.query;
            const { new_password, confirm_new_password } = req.body;

            console.log('TOKEN :', token);

            if (!token) return res.render('auth/reset-password', { message: 'invalid token', token });
            if (new_password != confirm_new_password) return res.render('auth/reset-password', { message: 'password doesn\'t match!', token });

            const payload = jwt.verify(token, JWT_SECRET_KEY);

            const encryptedPassword = await bcrypt.hash(new_password, 10);

            const user = await User.update({ password: encryptedPassword }, { where: { id: payload.user_id } });
            
            return res.render('auth/login', { error: null });
        } catch (err) {
            next(err);
        }
    },

    delete: async (req, res) => {
        try {
          const { id } = req.params;
          const user = await User_game.destroy({
            where: { id: id },
          });
          return res.status(200).json({
            status: "success",
            mesage: "Delete data success",
            data: user
          });
        } catch (err) {
          console.log(err);
        }
  },

    welcomeView: (req, res) => {
    return res.render("notification/index.ejs", { message: null });
  },
};