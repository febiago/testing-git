const { User_media } = require('../models');

module.exports = { 
    uploadImage: async (req, res, next) => { 
        try { 
            const user = req.user;
            const imageUrl = req.protocol + "://" + req.get('host') + "/media/" +  req.file.filename;
            await User_media.create({
                id_user : user.id,
                filename: req.file.filename,
                url: imageUrl
            });
            return res.status(200).json({
                status : true,
                url: imageUrl
            });
        } catch (err) { 
            next(err);
        }
    },
    uploadVideo: async (req, res, next) => {
        try { 
            const user = req.user;
            const videoUrl = req.protocol + "://" + req.get('host') + "/media/" +  req.file.filename;
            await User_media.create({
                id_user: user.id,
                filename: req.file.filename,
                url: videoUrl
            });
            return res.status(200).json({
                status : true,
                url: videoUrl
            });
        } catch (err) { 
            next(err);
        }
    },
 
    getMedia: async(req, res, next) => {
        try { 
            const { id } = req.params;
            const media = await User_media.findOne({ where: { id: id } });
            return res.status(200).json({
                status: true,
                message: "Success",
                media
            });
        } catch(err) { 
            next(err);
        }
    }
}
