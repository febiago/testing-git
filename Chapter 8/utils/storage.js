const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, './public/media');
    },

    // generete unique filename
    filename: (req, file, callback) => {
        const namaFile = Date.now() + path.extname(file.originalname);
        callback(null, namaFile);
    }
});


module.exports = {
    image: multer({
        storage: storage,

        // add file filter
        fileFilter: (req, file, callback) => {
            if (file.mimetype == 'image/png' || file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg') {
                callback(null, true);
            } else {
                const err = new Error('only png, jpg, and jpeg allowed to upload!');
                callback(err, false);
            }
        },

        // error handling
        onError: (err, next) => {
            next(err);
        }
    }),

    video: multer({
        storage: storage,

        // add file filter
        fileFilter: (req, file, callback) => {
            if (file.mimetype == 'video/mp4' || file.mimetype == 'video/x-matroska' || file.mimetype == 'video/x-msvideo') {
                callback(null, true);
            } else {
                const err = new Error('only mp4, mkv, and avi allowed to upload!');
                callback(err, false);
            }
        },

        // error handling
        onError: (err, next) => {
            next(err);
        }
    })

};