'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_media extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  User_media.init({
    id_user: DataTypes.INTEGER,
    filename: DataTypes.STRING,
    url: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User_media',
  });
  return User_media;
};