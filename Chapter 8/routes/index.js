const express = require('express');
const router = express.Router();
const c = require('../controllers');
const m = require('../helpers/middleware');
const storage = require('../utils/storage');

router.get('/auth/login/google', c.auth.google);
router.get('/welcome', c.auth.welcomeView);

router.post('/auth/register', c.auth.register);
router.post('/auth/login', c.auth.login);
router.get('/auth/profil',m.mustLogin, c.auth.profil);
router.put('/auth/changepass',m.mustAdmin, c.auth.changePassword);
router.get('/auth/forgot-password', m.mustLogin, c.auth.forgotPasswordView);
router.post('/auth/forgot-password', m.mustLogin, c.auth.forgotPassword);
router.get('/auth/reset-password', m.mustLogin, c.auth.resetPasswordView);
router.post('/auth/reset-password', m.mustLogin, c.auth.resetPassword);
router.delete('/auth/delete/:id',m.mustAdmin, c.auth.delete);

router.get('/biodata',m.mustLogin, c.biodata.read);
router.post('/biodata',m.mustAdmin, c.biodata.create);
router.put('/biodata/update/',m.mustAdmin, c.biodata.update);
router.delete('/biodata/delete/:id',m.mustAdmin, c.biodata.delete);

router.get('/history',m.mustLogin, c.history.read);
router.post('/history',m.mustAdmin, c.history.create);
router.put('/history/update',m.mustAdmin, c.history.update);
router.delete('/history/delete/:id',m.mustAdmin, c.history.delete);

router.post('/upload/image',m.mustAdmin, storage.image.single('media'), c.media.uploadImage);
router.post('/upload/video',m.mustAdmin, storage.video.single('media'), c.media.uploadVideo);
router.get('/media/:id',m.mustLogin, c.media.getMedia);


module.exports = router; 
