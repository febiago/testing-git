require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const router = require('./routes');
const Sentry = require('@sentry/node');
const Tracing = require("@sentry/tracing");
const app = express();

const {
    HTTP_PORT = 3000,
    ENVIRONMENT= 'production'
} = process.env;

app.use(express.json());
app.use(morgan('dev'));
app.use(router)

Sentry.init({
    dsn: 'https://4bb03c6ea0384849ab5f8040cb986fff@o4504078210170880.ingest.sentry.io/4504078234091521',
    environment: ENVIRONMENT,
    integrations: [
        new Sentry.Integrations.Http({ tracing: true }),
        new Tracing.Integrations.Express({
            app
        })
    ],
    sampleRate: 1.0
});

app.use(Sentry.Handlers.requestHandler());
app.use(Sentry.Handlers.tracingHandler());
app.use(morgan('dev'));

app.post("/notification/subscribe", async (req, res) => {
  try {
    const subscription = req.body;
    const payload = JSON.stringify({
      title: "Welcome",
      body: "Selamat Bergabung",
    });
    webpush
      .sendNotification(subscription, payload)
      .then((result) => console.log(result))
      .catch((e) => console.log(e.stack));

    res.status(200).json({ success: true });
  } catch (err) {
    console.log(err);
    return res.status(500).json({
      status: false,
      message: err.message,
    });
  }
});

app.use(Sentry.Handlers.errorHandler());
app.use(function onError(err, req, res, next) {
    return res.status(500).json({
        status: false,
        message: err.message,
        data: null
    });
});


// 404 handler
app.use((req, res, next) => {
    return res.status(404).json({
        status: false,
        message: 'Are you lost?'
    });
});

// 500 handler
app.use((err, req, res, next) => {
    console.log(err);
    return res.status(500).json({
        status: false,
        message: err.message
    });
});

app.listen(HTTP_PORT, () => console.log('berjalan pada port', HTTP_PORT));

