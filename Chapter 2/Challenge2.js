// Febi Ago
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let arrNilai = [];
console.log("Inputkan nilai dan ketik “q” jika sudah selesai")
rl.on("line", line => {
  let newLine = line.split(" ");
  newLine.map(line => arrNilai.push(line));
  if (line == "q")
    rl.close();
});

rl.on("close", function() {
arrNilai.pop();
// Nilai Tertinggi
const max = Math.max.apply(null, arrNilai);
console.log(`Nilai Tertinggi : ${max}`);

//Nilai Terendah
const min = Math.min.apply(null, arrNilai);
console.log(`Nilai Terendah : ${min}`);

// Rata - rata
const rata = arrNilai.reduce(function(a, b){return (+a + +b) / arrNilai.length});
console.log(`Rata - rata nilai : ${rata}`);

//Siswa Lulus Tidak Lulus
const lulus = arrNilai.filter(x => x >= 60).length;
const tdkLulus = arrNilai.filter(x => x < 60).length;
console.log(`Siswa lulus : ${lulus}, Siswa Tidak Lulus: ${tdkLulus}`);

//Urutan Nilai
const urutan = arrNilai.sort(function(a, b){return a-b});
console.log(`Urutan nilai dari terendah ke tertinggi : ${urutan}`);

// Siswa dengan nilai 90 dan 100
const nilaiS = arrNilai.filter(x => x == 90 || x == 100);
console.log(`Siswa Nilai 90 dan Nilai 100 : ${nilaiS}`);

});
