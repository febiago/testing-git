const { Collection, Item, Header} = require('postman-collection');
const fs = require('fs');

// Create Collection
const postmanCollection = new Collection({
    info: {
        name: 'Challenge 5 : Dokumentasi API Game'
    },
    item: []
});

// Set Header
const rawHeader = {
      "Content-type": "application/json",
    };
const loginHeader = {
      "Content-type": "application/json",
      "Authorization":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwibmFtZSI6IkZlYmkgQWdvIEpvbmF0YSIsInVzZXJuYW1lIjoiZmViaWFnbyIsImlhdCI6MTY2NTEzNzIyOX0.SicoLHwzUYvoCUbjr-l2IZlqAB4Kohbl4rCy20q_o_E",
    };

// Add test for request
const requestTest = `
pm.test('Sample test:  Tes for Succesfull respons', function(){
    pm.export(pm.respons.code).to.equal(200);
});
`

// Create the hedaer Auth
const reqRegister = new Item({
    name: 'Auth - Register',
    request: {
        header: rawHeader,
        url: 'http://localhost:3000/auth/register',
        method: 'POST',
        body: {
            mode: 'raw',
            raw: JSON.stringify({
                name: "Febi Ago Jonata",
                username: "febiago",
                password: "password"
            })
        },
        auth: null
    },
    events: [
        {
            listen: 'test',
            script: {
                type: 'text/javascript',
                exec: requestTest
            }
        }
    ]
});
postmanCollection.items.add(reqRegister)

const reqLogin = new Item({
    name: 'Auth - Login',
    request: {
        header: rawHeader,
        url: 'http://localhost:3000/auth/login',
        method: 'POST',
        body: {
            mode: 'raw',
            raw: JSON.stringify({
                username: "febiago",
                password: "password"
            })
        },
        auth: null
    },
    events: [
        {
            listen: 'test',
            script: {
                type: 'text/javascript',
                exec: requestTest
            }
        }
    ]
});
postmanCollection.items.add(reqLogin)

const Profil = new Item({
    name: 'Auth - Profil',
    request: {
        header: loginHeader,
        url: 'http://localhost:3000/auth/profil',
        method: 'GET',
        auth: null
    },
    events: [
        {
            listen: 'test',
            script: {
                type: 'text/javascript',
                exec: requestTest
            }
        }
    ]
});
postmanCollection.items.add(Profil)

const reqChangePass = new Item({
    name: 'Auth - Change Password',
    request: {
        header: loginHeader,
        url: 'http://localhost:3000/auth/changepass',
        method: 'PUT',
        body: {
            mode: 'raw',
            raw: JSON.stringify({
                oldPassword: "password",
                newPassword: "pass123",
                confirmNewPassword: "pass123"
            })
        },
        auth: null
    },
    events: [
        {
            listen: 'test',
            script: {
                type: 'text/javascript',
                exec: requestTest
            }
        }
    ]
});
postmanCollection.items.add(reqChangePass);

const reqDeleteUser = new Item({
    name: 'Auth - Delete User',
    request: {
        header: loginHeader,
        url: 'http://localhost:3000/auth/delete/',
        method: 'DELETE',
        auth: null
    },
    events: [
        {
            listen: 'test',
            script: {
                type: 'text/javascript',
                exec: requestTest
            }
        }
    ]
});
postmanCollection.items.add(reqDeleteUser);

// Header Biodata
const reqCreateBio = new Item({
    name: 'Biodata - Create',
    request: {
        header: loginHeader,
        url: 'http://localhost:3000/biodata',
        method: 'POST',
        body: {
            mode: 'raw',
            raw: JSON.stringify({
                id_user: "2",
                email: "febiago@mail.com",
                addres: "Pacitan"
            })
        },
        auth: null
    },
    events: [
        {
            listen: 'test',
            script: {
                type: 'text/javascript',
                exec: requestTest
            }
        }
    ]
});
postmanCollection.items.add(reqCreateBio);

const reqGetBio = new Item({
    name: 'Biodata - Read',
    request: {
        header: loginHeader,
        url: 'http://localhost:3000/biodata',
        method: 'GET',
        auth: null
    },
    events: [
        {
            listen: 'test',
            script: {
                type: 'text/javascript',
                exec: requestTest
            }
        }
    ]
});
postmanCollection.items.add(reqGetBio);

const reqUpdateBio = new Item({
    name: 'Biodata - Update',
    request: {
        header: loginHeader,
        url: 'http://localhost:3000/biodata/update',
        method: 'PUT',
        body: {
            mode: 'raw',
            raw: JSON.stringify({
                email: "febiago@mail.com",
                addres: "Pacitan, Jawa Timur"
            })
        },
        auth: null
    },
    events: [
        {
            listen: 'test',
            script: {
                type: 'text/javascript',
                exec: requestTest
            }
        }
    ]
});
postmanCollection.items.add(reqUpdateBio);

const reqDeleteBio = new Item({
    name: 'Biodata - Delete',
    request: {
        header: loginHeader,
        url: 'http://localhost:3000/biodata/delete/',
        method: 'DELETE',
        auth: null
    },
    events: [
        {
            listen: 'test',
            script: {
                type: 'text/javascript',
                exec: requestTest
            }
        }
    ]
});
postmanCollection.items.add(reqDeleteBio);

// Header history
const reqCreateHis = new Item({
    name: 'History - Create',
    request: {
        header: loginHeader,
        url: 'http://localhost:3000/history',
        method: 'POST',
        body: {
            mode: 'raw',
            raw: JSON.stringify({
                id_user: "2",
                level: "9",
                skill: "Fire Ball"
            })
        },
        auth: null
    },
    events: [
        {
            listen: 'test',
            script: {
                type: 'text/javascript',
                exec: requestTest
            }
        }
    ]
});
postmanCollection.items.add(reqCreateHis);

const reqGetHis = new Item({
    name: 'History - Read',
    request: {
        header: loginHeader,
        url: 'http://localhost:3000/history',
        method: 'GET',
        auth: null
    },
    events: [
        {
            listen: 'test',
            script: {
                type: 'text/javascript',
                exec: requestTest
            }
        }
    ]
});
postmanCollection.items.add(reqGetHis);

const reqUpdateHis = new Item({
    name: 'History - Update',
    request: {
        header: loginHeader,
        url: 'http://localhost:3000/history/update/',
        method: 'PUT',
        body: {
            mode: 'raw',
            raw: JSON.stringify({
                level: "10",
                skill: "Fire Ball"
            })
        },
        auth: null
    },
    events: [
        {
            listen: 'test',
            script: {
                type: 'text/javascript',
                exec: requestTest
            }
        }
    ]
});
postmanCollection.items.add(reqUpdateHis);

const reqDeleteHis = new Item({
    name: 'History - Delete',
    request: {
        header: loginHeader,
        url: 'http://localhost:3000/history/delete/',
        method: 'DELETE',
        auth: null
    },
    events: [
        {
            listen: 'test',
            script: {
                type: 'text/javascript',
                exec: requestTest
            }
        }
    ]
});
postmanCollection.items.add(reqDeleteHis);

// Generate to JSON
const collectionJSON = postmanCollection.toJSON();
// Export to File
fs.writeFile('./collection.json', JSON.stringify(collectionJSON), (err) => { 
    if (err) console.log(err);
    console.log('File Save');
});