const auth = require('./auth');
const biodata = require('./biodata');
const history = require('./history');
const media = require('./media');

module.exports = { auth, biodata, history, media};