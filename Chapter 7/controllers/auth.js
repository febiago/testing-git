const { User_game } = require('../models');
const googleOauth2 = require('../utils/google');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { gmail } = require('googleapis/build/src/apis/gmail');
const { use } = require('../routes');

const {
    JWT_SIGNATURE_KEY
} = process.env;

module.exports = {
    google: async (req, res, next) => {
        try {
            const code = req.query.code;

            // form login jika code tidak ada
            if (!code) {
                const url = googleOauth2.generateAuthURL();
                return res.redirect(url);
            }
            await googleOauth2.setCredentials(code);
            const { data } = await googleOauth2.getUserData();
            const username= data.email.replace('@gmail.com', '');
            var userExist = await User_game.findOne({ where: { username: username } });

            const encryptedPassword = await bcrypt.hash('pass123', 10);

            if (!userExist) {
                userExist = await User_game.create({
                    name: data.name,
                    username: username,
                    password: encryptedPassword,
                    user_type: 'google',
                    role: 'user'
                });
            }
            // generate token
            const payload = {
                id: userExist.id,
                name: userExist.name,
                username: userExist.username,
                role: userExist.role,
            };
            const token = jwt.sign(payload, JWT_SIGNATURE_KEY);

            // return token 
            return res.status(200).json({
                status: true,
                message: 'success',
                data: {
                    id_user: userExist.id,
                    token
                }
            });
        } catch (err) {
            next(err);
        }
    },

    register: async (req, res, next) => {
        try {
            const { name, username, password, user_type, role } = req.body;

            const existUser = await User_game.findOne({ where: { username: username } });
            if (existUser) {
                return res.status(409).json({
                    status: false,
                    message: 'username already used!'
                });
            }

            const encryptedPassword = await bcrypt.hash(password, 10);
            const user = await User_game.create({
                name,
                username,
                password: encryptedPassword,
                user_type,
                role
            });

            return res.status(201).json({
                status: true,
                message: 'success',
                data: {
                    name: user.name,
                    username: user.username,
                    role: user.role
                }
            });
        } catch (err) {
            next(err);
        }
    },

    login: async (req, res, next) => {
        try {
            const { username, password } = req.body;

            const user = await User_game.findOne({ where: { username: username } });
            if (!user) {
                return res.status(400).json({
                    status: false,
                    message: 'username or password doesn\'t match!'
                });
            }

            const correct = await bcrypt.compare(password, user.password);
            if (!correct) {
                return res.status(400).json({
                    status: false,
                    message: 'user or password doesn\'t match!'
                });
            }
            // generate token
            payload = {
                id: user.id,
                name: user.name,
                username: user.username,
                user_type: user.user_type,
                role: user.role
            };
            const token = jwt.sign(payload, JWT_SIGNATURE_KEY);
            const decoded = jwt.verify(token, JWT_SIGNATURE_KEY);
            req.id = decoded;

            if (decoded.user_type == "google") {
              return res.status(403).json({
                status: false,
                message: "This user is logged in using a google account!",
              });
            }
            return res.status(200).json({
                status: true,
                message: 'success',
                data: {
                    token: token
                }
            });
        } catch (err) {
            next(err);
        }
    },

    profil: (req, res, next) => {
        const user = req.user;

        try {
            return res.status(200).json({
                status: true,
                message: 'success',
                data: user
            });
        } catch (err) {
            next(err);
        }
    },

    changePassword: async (req, res, next) => {
            try {
                const { oldPassword, newPassword, confirmNewPassword } = req.body;
                
                if (newPassword !== confirmNewPassword) {
                    return res.status(422).json({
                        status: false,
                        message: 'new password and confirm new password doesnt match!'
                    });
                }
    
                const user = await User_game.findOne({ where: { id: req.user.id } });
                if (!user) 
                    return res.status(404).json({ 
                        success: false, 
                        message: 'User not found!' 
                    });
                
                const correct = await bcrypt.compare(oldPassword, user.password, );
                if (!correct) {
                    return res.status(400).json({
                        status: false,  
                        message: 'old password does not match!'
                    });
                }
    
                const encryptedPassword = await bcrypt.hash(newPassword, 10);
                const updatedUser = await User_game.update({
                    password: encryptedPassword
                }, {
                    where: {
                        id: user.id
                    }
                });
    
                return res.status(200).json({
                    status: true,
                    message: 'success',
                    data: updatedUser
                });
    
            } catch (err) {
                next(err);
            }
},

    delete: async (req, res) => {
        try {
          const { id } = req.params;
          const user = await User_game.destroy({
            where: { id: id },
          });
          return res.status(200).json({
            status: "success",
            mesage: "Delete data success",
            data: user
          });
        } catch (err) {
          console.log(err);
        }
  }
};