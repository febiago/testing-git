-- DDL Membuat Tabel
CREATE TABLE user (
  id bigserial PRIMARY KEY, 
  name varchar(50), 
  email varchar(30),
  password varchar(15)
);

CREATE TABLE payment (
  id bigserial PRIMARY KEY, 
  user_id integer, 
  name varchar(50)
);

CREATE TABLE products (
  id bigserial PRIMARY KEY, 
  name varchar(50), 
  price integer,
  image varchar(200),
  description text,
  stock integer
);

CREATE TABLE orders (
  id bigserial PRIMARY KEY, 
  user_id integer,
  product_id integer,
  payment_id integer,  
  address varchar(100), 
  date date,
  qty integer(200),
  status varchar(20)
);

-- DML Memasukan data ke dalam tabel
INSERT INTO user (name, email, password) 
VALUES ('Febi Ago', 'febiago3@gmail.com','paswordku'), 
('Jonata', 'jonata@gmail.com','1280100');

INSERT INTO payment (user_id, name) VALUES (1, 'Bank BRI');

INSERT INTO products (name, price, image, description, stock) 
VALUES ('Samsung S10', 7000000, 's10.jpg', 'Fullset No Minus', 1), 
('Xiaomi A2', 1400000, 'xaiomia2.jpg', 'Hp Cas No Minus', 2);

INSERT INTO orders (user_id,product_id,payment_id,address,date,qty,status) 
VALUES (1, 2, 1, 'Pacitan, Jawa Timur', 2022-09-09, 1, Belum Dibayar);

-- DML Membaca/Membuka isi Tabel
SELECT * FROM user;

-- DML Update isi Tabel orders dengan id 1
UPDATE orders SET status = 'Barang Dikirim' WHERE id = 1;

-- DML Menghapus data user dengan id 2
DELETE FROM user WHERE id = 2;
