const readline = require('readline');

const interface = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function input(q) {
    return new Promise(resolve => {
        interface.question(q, data =>{
            return resolve(data);
        })
    })
}

function hitung(a1, op, a2) {
	if(op === "+") return a1 + a2;
	else if(op === "-") return a1 - a2;
	else if(op === "*") return a1 * a2;
	else if(op === "/") return a1 / a2;
}

const luasPersegi = (s) => s*s;
const volKubus = (s) => s**3;
const volTabung = (r, t) => 3.14 * r**2 * t;

console.log("== Pilih Menu ==")
console.log("1. Hitung (+)(-)(*)(/)")
console.log("2. Akar Kuadrat")
console.log("3. Luas Persegi")
console.log("4. Volume Kubus")
console.log("5. Volume Tabung")
console.log("================")

async function main() {
    try {
        let menu = await input("Pilih Menu No: ");
        let pilihMenu = parseInt(menu);

        if (pilihMenu == 1) {
          let a1 = await input("Angka 1: ");
          let op = await input("Operator: ");
          let a2 = await input("Angka 2: ");
          const hasil = hitung(Number(a1), op, Number(a2));
          console.log(`Hasil : ${hasil}`);
          interface.close();

        }else if(pilihMenu == 2){
          let a3 = await input("Masukan Angka: ");
          const akar = Math.sqrt(Number(a3));
          console.log(`Akar kuadrat dari ${a3} adalah : ${akar}`);
          interface.close();

        }else if(pilihMenu == 3) {
          let s1 = await input("Sisi: ");
          const hasilPersegi = luasPersegi(Number(s1));
          console.log(`Luas Persegi : ${hasilPersegi}`);
          interface.close();

        }else if(pilihMenu == 4) {
          let s2 = await input("Sisi: ");
          const hasilKubus = volKubus(Number(s2));
          console.log(`Volume Kubus : ${hasilKubus}`);
          interface.close();

        }else if(pilihMenu == 5) {
          let r = await input("Jari - Jari: ");
          let t = await input("Tinggi: ");
          const hasilTabung = volTabung(Number(r),Number(t));
          console.log(`Volume Tabung : ${hasilTabung}`);
          interface.close();

        }else{
          console.log("Menu yang dipilih tidak ada");
          interface.close();
        }
        
        }catch (err) {
            console.log(err);
        }
    }

main();